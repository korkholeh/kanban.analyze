## trello_statistics.py

This is a little script for analyze the Trello boards in kanban style.

Go to your board and select `Options -> Share, Print and Export`:

![](https://dl.dropbox.com/u/381385/devblog/Selection_012.png)

Then select `Export JSON`

![](https://dl.dropbox.com/u/381385/devblog/Selection_013.png)

and save `.json` file to the local machine.

The next command will generate the `.csv` file as result of board analyzing:

```
$ ./trello_statistics.py my_board.json
```

CSV file will be saved to the same directory where original file is.

![](https://dl.dropbox.com/u/381385/devblog/Workspace%201_014.png)

The result contain the next columns:

* Card name
* Assigned members
* Date where the card was added to the board
* Date where the card was placed to `Ready` list
* Time delta between these dates in days

