#!/usr/bin/python
# coding: utf-8
from __future__ import unicode_literals

import os
import sys
import json
import csv

from datetime import datetime, timedelta

def analyze_file(filename):
    data = json.load(open(filename, 'r'))
    print(data['name'])
    print('-'*len(data['name']))
    
    members = data['members']
    for item in members:
        print (item['fullName'], item['id'])

    print('*'*10)
    for item in data['lists']:
        print item['name'], item['id']

    last_list_id = data['lists'][-1]['id']

    cards = [card for card in data['cards'] if card['idList'] == last_list_id]
    card_ids = [card['id'] for card in cards]

    # for card in cards:
    #     print card['name']
    #     print card['dateLastActivity']
    #     print card['id']

    actions = [a for a in data['actions'] if 'card' in a['data'] and a['data']['card']['id'] in card_ids]
    print '-'*10

    csvname = os.path.splitext(filename)[0] + '.csv'
    with open(csvname, 'wb') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=b',',
                            quotechar=b'"', quoting=csv.QUOTE_MINIMAL)
        for card in cards:
            card_actions = filter(lambda x:x['data']['card']['id'] == card['id'], actions)
            if card_actions:
                print card['name']
                first_action_date = datetime.strptime(card_actions[0]['date'], '%Y-%m-%dT%H:%M:%S.%fZ')
                last_action_date = datetime.strptime(card_actions[-1]['date'], '%Y-%m-%dT%H:%M:%S.%fZ')
                delta = first_action_date-last_action_date
                names = ', '.join(map(lambda t:t['fullName'],
                    filter(lambda x:x['id'] in card['idMembers'], members))) or 'Unknown'
                print last_action_date, first_action_date, delta.days
                csvwriter.writerow([
                        card['name'].encode('utf-8'), 
                        names.encode('utf-8'), 
                        last_action_date, 
                        first_action_date, 
                        delta.days])
            # else:
            #     print('No actions')

    # for action in actions[:1]:
    #     print action['date']
    #     print action['data']


if __name__ == '__main__':
    for arg in sys.argv[1:]:
        if os.path.exists(arg):
            analyze_file(arg)
        else:
            print('File "{0}" does not exists.'.format(arg))
